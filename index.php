<!DOCTYPE html>
<html>
    <head>
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.35/css/uikit.min.css" />
        
        <!-- UIkit JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.35/js/uikit.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.35/js/uikit-icons.min.js"></script>
        
        <!-- Angular JS 1.6.6 -->
        <script type="text/javascript" src="libs/angular-1.6.6/angular.min.js"></script>
        <!--<script type="text/javascript" src="libs/angular-1.6.6/angular-route.min.js"></script>-->
        
        <!-- ui-router -->
	    <script src="//unpkg.com/@uirouter/angularjs@1.0.7/release/angular-ui-router.min.js"></script>
        
        <!-- Controllers -->
        <script type="text/javascript" src="src/home/home.js"></script>
        <script type="text/javascript" src="src/canal/cadastrar.js"></script>
        
        <!-- APP -->
        <script type="text/javascript" src="app/module.js"></script>
        <script type="text/javascript" src="app/controller.js"></script>
        <script type="text/javascript" src="app/route.js"></script>
        
        
    </head>

    <body ng-app='app' ng-controller='appCtrl'>
        <nav class="uk-navbar-container" uk-navbar>
            <div class="uk-navbar-left">
        
                <ul class="uk-navbar-nav">
                    <li class="uk-active"><a href="#">Divulgue</a></li>
                    <li>
                        <a href="#">Seu canal</a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                <li class="uk-active"><a href="#">Inscritos</a></li>
                                <li><a href="#">Vídeos</a></li>
                                <li><a href="#">Parceiros</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="#">Teste</a></li>
                </ul>
        
            </div>
        </nav>
        
        <div ui-view></div>
        
    </body>
</html>
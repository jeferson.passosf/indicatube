var urlLocation = window.location.pathname.toString();


app.config( function($stateProvider, $urlRouterProvider){
    
    // $locationProvider.html5Mode(false);
    // $locationProvider.hashPrefix('');
        
    $stateProvider
    .state('home', {
    	url : '/',
    	templateUrl : 'src/home/home.html',
        // template: "{{test}}",
    	controller : homeCtrl
    })
    .state('cadastrar canal', {
        url: '/canal/cadastrar',
        templateUrl: 'src/canal/cadastrar.html',
        controller: divulgarCtrl
    });
        
    $urlRouterProvider.otherwise('/');
})


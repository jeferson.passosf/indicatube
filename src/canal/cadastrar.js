var divulgarCtrl = function($scope, $http){
    
    var g_key_api = "AIzaSyBjacjeA6VjQf6O-ZRsIA6UHVTvhxPAAXM";
    var youtubeApiUrl = "https://www.googleapis.com/youtube/v3/channels/?&part=contentDetails%2CbrandingSettings%2CcontentOwnerDetails%2Cid%2CinvideoPromotion%2Clocalizations%2Csnippet%2Cstatistics%2Cstatus%2CtopicDetails&key=" + g_key_api;
    
    
    $scope.idChannel = "";
    $scope.userChannel = "";
    
    $scope.channelInfo = {};
    $scope.userInfo = {};
    
    $http({
        method: "GET",
        url: "https://api.whatsapp.com/send?phone=11950004234&message=TESTE"
    }).then((response) => {
        console.log(response.data);
    })
    
    function searchChannel(param){
        
        
        
        $http({
            'method': "GET",
            url: youtubeApiUrl + param
        }).then( (response)=>{
            $scope.idChannel = response.data.items[0].id;
            $scope.channelInfo = response.data;

            console.log(response.data);
        }, httpError);
        
    }
    
    $scope.searchChannel = (id, user) => {
        
        var param = "";
        
        if(id != ""){
            param = "&id=" + id;
        }
        
        if( user != "" ){
            if($scope.idChannel != "")
                $scope.idChannel = "";
            param = "&forUsername=" + user;
        }
        
        if(param)
            searchChannel(param);
        else
            $scope.messageSearch = "Usuário ou Id não encontrado";
    };
    
    // searchChannel('UCDeLXkY4N3bTm15nIpXgtQA');
    
    var httpError = (response) => {
        console.error(response);
    }
    
// response API example 
/**
 * API response
 */
$scope.channelInfo = {
  "kind": "youtube#channelListResponse",
  "etag": "\"S8kisgyDEblalhHF9ooXPiFFrkc/K5km8plPlkyfiEK33msYKnqfpxg\"",
  "pageInfo": {
    "totalResults": 1,
    "resultsPerPage": 1
  },
  "items": [
    {
      "kind": "youtube#channel",
      "etag": "\"S8kisgyDEblalhHF9ooXPiFFrkc/YO-6Qv-zUItBYPBIfrkj6nwrfHs\"",
      "id": "UCDeLXkY4N3bTm15nIpXgtQA",
      "snippet": {
        "title": "Programming BR",
        "description": "Aqui você encontra tutoriais , gameplays e analises e muitas outras curiosidades... inscreva-se para receber tudo isso na sua pagina inicial.\n\nProdutores do canal:\nJeferson Fiuza e Renato Sanches\n\nEquipamentos de gravação :\n\nNotbook, PS3, Galaxy, Câmera Samsung ST77\n\nEditores utilizados:\n\nSony Vegas Pro 11\n\nContatos - \nSkype : jefynho121\nEmail : coolgamesjr@outlook.com\nFan Page : https://www.facebook.com/ajudatutoriais\nTwitter : https://twitter.com/CoolGamesJR\n\nFique ligado no canal!!!!",
        "customUrl": "programmingbr",
        "publishedAt": "2011-06-07T10:39:46.000Z",
        "thumbnails": {
          "default": {
            "url": "https://yt3.ggpht.com/-mzWAHosuIjM/AAAAAAAAAAI/AAAAAAAAAAA/w3SbgeHTCFs/s88-c-k-no-mo-rj-c0xffffff/photo.jpg"
          },
          "medium": {
            "url": "https://yt3.ggpht.com/-mzWAHosuIjM/AAAAAAAAAAI/AAAAAAAAAAA/w3SbgeHTCFs/s240-c-k-no-mo-rj-c0xffffff/photo.jpg"
          },
          "high": {
            "url": "https://yt3.ggpht.com/-mzWAHosuIjM/AAAAAAAAAAI/AAAAAAAAAAA/w3SbgeHTCFs/s240-c-k-no-mo-rj-c0xffffff/photo.jpg"
          }
        },
        "localized": {
          "title": "Programming BR",
          "description": "Aqui você encontra tutoriais , gameplays e analises e muitas outras curiosidades... inscreva-se para receber tudo isso na sua pagina inicial.\n\nProdutores do canal:\nJeferson Fiuza e Renato Sanches\n\nEquipamentos de gravação :\n\nNotbook, PS3, Galaxy, Câmera Samsung ST77\n\nEditores utilizados:\n\nSony Vegas Pro 11\n\nContatos - \nSkype : jefynho121\nEmail : coolgamesjr@outlook.com\nFan Page : https://www.facebook.com/ajudatutoriais\nTwitter : https://twitter.com/CoolGamesJR\n\nFique ligado no canal!!!!"
        }
      },
      "contentDetails": {
        "relatedPlaylists": {
          "favorites": "FLDeLXkY4N3bTm15nIpXgtQA",
          "uploads": "UUDeLXkY4N3bTm15nIpXgtQA",
          "watchHistory": "HL",
          "watchLater": "WL"
        }
      },
      "statistics": {
        "viewCount": "792466",
        "commentCount": "4",
        "subscriberCount": "4213",
        "hiddenSubscriberCount": false,
        "videoCount": "93"
      },
      "topicDetails": {
        "topicIds": [
          "/m/0bzvm2",
          "/m/07c1v",
          "/m/019_rr"
        ],
        "topicCategories": [
          "https://en.wikipedia.org/wiki/Video_game_culture",
          "https://en.wikipedia.org/wiki/Technology",
          "https://en.wikipedia.org/wiki/Lifestyle_(sociology)"
        ]
      },
      "status": {
        "privacyStatus": "public",
        "isLinked": true,
        "longUploadsStatus": "longUploadsUnspecified"
      },
      "brandingSettings": {
        "channel": {
          "title": "Programming BR",
          "description": "Aqui você encontra tutoriais , gameplays e analises e muitas outras curiosidades... inscreva-se para receber tudo isso na sua pagina inicial.\n\nProdutores do canal:\nJeferson Fiuza e Renato Sanches\n\nEquipamentos de gravação :\n\nNotbook, PS3, Galaxy, Câmera Samsung ST77\n\nEditores utilizados:\n\nSony Vegas Pro 11\n\nContatos - \nSkype : jefynho121\nEmail : coolgamesjr@outlook.com\nFan Page : https://www.facebook.com/ajudatutoriais\nTwitter : https://twitter.com/CoolGamesJR\n\nFique ligado no canal!!!!",
          "keywords": "\"ajuda tutoriais cool games gamepalys\" programing \"programação programacao\" \"linguagem c\"",
          "trackingAnalyticsAccountId": "UA-47444207-2",
          "showRelatedChannels": true,
          "showBrowseView": true,
          "featuredChannelsTitle": "Recomendações",
          "featuredChannelsUrls": [
            "UCldjCveGLU_OjE9altJn9bg",
            "UCVkGpSiopT5uJNB50trojSw",
            "UCaRcy5IZkkjTO1vd2uKpwqA"
          ],
          "unsubscribedTrailer": "rOS378Son9o",
          "profileColor": "#000000"
        },
        "image": {
          "bannerImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1060-fcrop64=1,00005a57ffffa5a8-nd-c0xffffffff-rj-k-no",
          "bannerMobileImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w640-fcrop64=1,32b75a57cd48a5a8-nd-c0xffffffff-rj-k-no",
          "bannerTabletLowImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1138-fcrop64=1,00005a57ffffa5a8-nd-c0xffffffff-rj-k-no",
          "bannerTabletImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1707-fcrop64=1,00005a57ffffa5a8-nd-c0xffffffff-rj-k-no",
          "bannerTabletHdImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w2276-fcrop64=1,00005a57ffffa5a8-nd-c0xffffffff-rj-k-no",
          "bannerTabletExtraHdImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w2560-fcrop64=1,00005a57ffffa5a8-nd-c0xffffffff-rj-k-no",
          "bannerMobileLowImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w320-fcrop64=1,32b75a57cd48a5a8-nd-c0xffffffff-rj-k-no",
          "bannerMobileMediumHdImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w960-fcrop64=1,32b75a57cd48a5a8-nd-c0xffffffff-rj-k-no",
          "bannerMobileHdImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1280-fcrop64=1,32b75a57cd48a5a8-nd-c0xffffffff-rj-k-no",
          "bannerMobileExtraHdImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1440-fcrop64=1,32b75a57cd48a5a8-nd-c0xffffffff-rj-k-no",
          "bannerTvImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w2120-fcrop64=1,00000000ffffffff-nd-c0xffffffff-rj-k-no",
          "bannerTvLowImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w854-fcrop64=1,00000000ffffffff-nd-c0xffffffff-rj-k-no",
          "bannerTvMediumImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1280-fcrop64=1,00000000ffffffff-nd-c0xffffffff-rj-k-no",
          "bannerTvHighImageUrl": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w1920-fcrop64=1,00000000ffffffff-nd-c0xffffffff-rj-k-no"
        },
        "hints": [
          {
            "property": "channel.featured_tab.template.string",
            "value": "Everything"
          },
          {
            "property": "channel.modules.show_comments.bool",
            "value": "True"
          },
          {
            "property": "channel.banner.mobile.medium.image.url",
            "value": "https://yt3.ggpht.com/wp5_jewYA4zNi2X8XzKzOHg2GntRaVVduYEl8zUNPeWiy4j-2D3BmYvMvgY60nTwI3bOkvGHJQ=w640-fcrop64=1,32b75a57cd48a5a8-nd-c0xffffffff-rj-k-no"
          }
        ]
      }
    }
  ]
}
}



create table channel(
    id int not null,
    channel_id varchar(100) not null,
    title varchar(100) not null,
    description varchar(2000),
    subscriberCount varchar(10),
    email varchar(100) not null,
    score varchar(15) not null,
    thumbnailUrl varchar(2000),
    viewsCount varchar(15),
    videoCount varchar(10)
)

create table share (
    id int not null,
    channelId int not null,
    channelIdShared int not null,
    foreing key channelId reference channel(id) 
    foreing key channelIdShared reference channel(id)
)


create table video (
    id int not null,
    thumbnailUrl varchar(2000),
    title varchar(200),
    channelId varchar(100) not null
)